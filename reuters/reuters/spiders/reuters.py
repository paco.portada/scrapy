import scrapy

class ReutersSpider(scrapy.Spider):
    name = 'reuters'

    allowed_domains = ['reuters.com']
    start_urls = [f'https://www.reuters.com/world/africa']

    def parse(self, response):

        # get the links of all relevant stories
        article_urls = response.xpath(f'//a[contains(@href, "/world/africa")]/@href').getall()
        
        for url in article_urls:
            item = {}
            item['url'] = 'https://www.reuters.com' + url

            # send the next Request to crawl more information at article level
            # for example: author, title, published time, updated time, and body
            yield scrapy.Request(item['url'],
                                 callback=self.parse_article,
                                 meta={'item':item})

    def parse_article(self, response):

        # item with url information is brought over from previous Response
        item = response.meta['item']

        # scrape information using the header meta field
        item['title']          = response.xpath('//meta[@property="og:title"]/@content').get()
        item['author']         = response.xpath('//meta[@name="article:author"]/@content').get()
        item['published_time'] = response.xpath('//meta[@name="article:published_time"]/@content').get()
        item['updated_time']   = response.xpath('//meta[@name="article:modified_time"]/@content').get()
        item['image']          = response.xpath('//meta[@property="og:image"]/@content').get()

        # scrape body
        item['body'] = response.xpath('//p[contains(@data-testid,"paragraph")]/text()').getall()

        # print out the item
        print(item)
        
        yield item
