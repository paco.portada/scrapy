import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes1"
    start_urls = [
        'https://quotes.toscrape.com/page/1/',
        'https://quotes.toscrape.com/page/2/',
    ]

    def parse(self, response):
        page = response.url.split("/")[-2]
        print('Página: ' + page)
        print()
        filename = f'quotes-{page}.html'
        with open(filename, 'wb') as f:
            f.write(response.body)
