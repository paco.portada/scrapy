import scrapy
from zara.items import Producto
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request

class ZaraSpider(scrapy.Spider):
    # Nombre de la araña
    name = "zara"
    
    # Dominios permitidos
    allowed_domains = ['zara.com']
    
    # URLs para comenzar a rastrear
    start_urls = [
		'https://www.zara.com/es/es/camiseta-basica-p05644101.html',
		'https://www.zara.com/es/es/pantalon-tiro-alto-p07901432.html'
    ]
    
    def parse(self, response):
        producto = Producto()

        # Extraemos los ennlaces
        links = LinkExtractor(
            allow_domains=['zara.com'],
            restrict_xpaths=["//a"],
            allow="/es/"
            ).extract_links(response)

        outlinks = []  # Lista con todos los enlaces
        for link in links:
            url = link.url
            outlinks.append(url) # Añadimos el enlace en la lista
            yield Request(url, callback=self.parse) # Generamos la petición

        product = response.xpath('//meta[@content="product"]').extract()
        if product:
            # Extraemos la url, el nombre del producto, la descripcion y su precio
            producto['url'] = response.request.url
            producto['nombre'] = response.xpath('//h1[@class="product-detail-info__header-name"]/text()').extract_first()
            producto['precio'] = response.xpath('//span[@class="price-current__amount"]/text()').extract_first()
            description = response.xpath('//div[@class="expandable-text__inner-content"]')
            producto['descripcion'] = description.xpath('//p/text()').extract_first()
            producto['links'] = outlinks
            
            yield producto
